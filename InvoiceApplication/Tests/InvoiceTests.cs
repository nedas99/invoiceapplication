using Xunit;
using NSubstitute;
using Repositories.Interfaces;
using NSubstitute.ReturnsExtensions;
using Services.Services;
using Models.Api.Invoices;
using System.Collections.Generic;
using System.Threading.Tasks;
using Models.Exceptions;
using Models.Database;
using Models.Enums;
using System.Linq;
using System;

namespace Tests
{
    public class InvoiceTests
    {
        [Fact]
        public async Task GetInvoice_EmptyPeopleAndProductRepositories_ShouldThrowPersonNotFound()
        {
            // Arrange:

            var productRepository = Substitute.For<IProductRepository>();
            var personRepository = Substitute.For<IPersonRepository>();

            personRepository.GetById(1).ReturnsNull();

            var invoiceService = new InvoiceService(personRepository, productRepository);
            var request = new InvoiceRequest
            {
                SellerId = 1,
                BuyerId = 2,
                Products = new List<ProductAmountDTO> 
                {
                    new ProductAmountDTO { ProductId = 1, Quantity = 1 }
                }
            };

            var expected = AppException.PersonNotFound();

            // Act:

            var exception = await Assert.ThrowsAsync<AppException>(() => invoiceService.GetInvoice(request));

            // Assert:

            Assert.Equal(expected.Message, exception.Message);
            Assert.Equal(expected.StatusCode, exception.StatusCode);
        }

        [Fact]
        public async Task GetInvoice_SellerIsIndividual_ShouldThrowInvalidSeller()
        {
            // Arrange:

            var productRepository = Substitute.For<IProductRepository>();

            var personRepository = Substitute.For<IPersonRepository>();

            personRepository.GetById(1)
                .Returns(Task.FromResult(new Individual() as Person));

            var invoiceService = new InvoiceService(personRepository, productRepository);
            var request = new InvoiceRequest
            {
                SellerId = 1,
                BuyerId = 2,
                Products = new List<ProductAmountDTO>
                {
                    new ProductAmountDTO { ProductId = 1, Quantity = 1 }
                }
            };

            var expected = AppException.InvalidSeller();

            // Act:

            var exception = await Assert.ThrowsAsync<AppException>(() => invoiceService.GetInvoice(request));

            // Assert:

            Assert.Equal(expected.Message, exception.Message);
            Assert.Equal(expected.StatusCode, exception.StatusCode);
        }

        [Fact]
        public async Task GetInvoice_MissingProducts_ShouldThrowProductNotFound()
        {
            // Arrange:

            var productRepository = Substitute.For<IProductRepository>();

            productRepository.GetProductsByIds(Arg.Is<List<int>>(x => x[0] == 1 && x[1] == 2))
                .Returns(Task.FromResult(new List<Product>
                {
                    new Product
                    {
                        Id = 1,
                        Title = "Title",
                        PriceWithoutVAT = 2m,
                        VAT = VAT.TwentyOne
                    }
                } as IEnumerable<Product>));

            var personRepository = Substitute.For<IPersonRepository>();

            personRepository.GetById(1)
                .Returns(Task.FromResult(new LegalPerson() as Person));

            personRepository.GetById(2)
                .Returns(Task.FromResult(new Individual() as Person));

            var invoiceService = new InvoiceService(personRepository, productRepository);
            var request = new InvoiceRequest
            {
                SellerId = 1,
                BuyerId = 2,
                Products = new List<ProductAmountDTO>
                {
                    new ProductAmountDTO { ProductId = 1, Quantity = 1 },
                    new ProductAmountDTO { ProductId = 2, Quantity = 2 }
                }
            };

            var expected = AppException.ProductNotFound();

            // Act:

            var exception = await Assert.ThrowsAsync<AppException>(() => invoiceService.GetInvoice(request));

            // Assert:

            Assert.Equal(expected.Message, exception.Message);
            Assert.Equal(expected.StatusCode, exception.StatusCode);
        }

        [Fact]
        public async Task GetInvoice_BoughtByIndividual_ProductShouldHaveVATSum()
        {
            // Arrange:

            var productRepository = Substitute.For<IProductRepository>();

            productRepository.GetProductsByIds(Arg.Is<IEnumerable<int>>(x => x.First() == 1))
                .Returns(Task.FromResult(new List<Product>
                {
                    new Product
                    {
                        Id = 1,
                        Title = "Title",
                        PriceWithoutVAT = 2m,
                        VAT = VAT.TwentyOne
                    }
                } as IEnumerable<Product>));

            var personRepository = Substitute.For<IPersonRepository>();

            personRepository.GetById(1)
                .Returns(Task.FromResult(new LegalPerson 
                { 
                    Id = 1,
                    Address = "Address",
                    Name = "Name Surname",
                    VATCode = "VAT code",
                    Code = "Code"
                } as Person));

            personRepository.GetById(2)
                .Returns(Task.FromResult(new Individual
                {
                    Id = 2,
                    Address = "Address",
                    Name = "Name Surname",
                    VATCode = "VAT code",
                    Code = "Code"
                } as Person));

            var invoiceService = new InvoiceService(personRepository, productRepository);

            var request = new InvoiceRequest
            {
                SellerId = 1,
                BuyerId = 2,
                Products = new List<ProductAmountDTO>
                {
                    new ProductAmountDTO { ProductId = 1, Quantity = 1 }
                }
            };

            // Act:

            var invoice = await invoiceService.GetInvoice(request);

            // Assert:

            Assert.IsType<Invoice>(invoice);
            Assert.NotNull(invoice.Products.First().VATSum);
        }

        [Fact]
        public async Task GetInvoice_BoughtByLegalPerson_ProductShouldNotHaveVATSum()
        {
            // Arrange:

            var productRepository = Substitute.For<IProductRepository>();

            productRepository.GetProductsByIds(Arg.Is<IEnumerable<int>>(x => x.First() == 1))
                .Returns(Task.FromResult(new List<Product>
                {
                    new Product
                    {
                        Id = 1,
                        Title = "Title",
                        PriceWithoutVAT = 2m,
                        VAT = VAT.TwentyOne
                    }
                } as IEnumerable<Product>));

            var personRepository = Substitute.For<IPersonRepository>();

            personRepository.GetById(1)
                .Returns(Task.FromResult(new LegalPerson
                {
                    Id = 1,
                    Address = "Address",
                    Name = "Name Surname",
                    VATCode = "VAT code",
                    Code = "Code"
                } as Person));

            personRepository.GetById(2)
                .Returns(Task.FromResult(new LegalPerson
                {
                    Id = 2,
                    Address = "Address",
                    Name = "Name Surname",
                    VATCode = "VAT code",
                    Code = "Code"
                } as Person));

            var invoiceService = new InvoiceService(personRepository, productRepository);
            var request = new InvoiceRequest
            {
                SellerId = 1,
                BuyerId = 2,
                Products = new List<ProductAmountDTO>
                {
                    new ProductAmountDTO { ProductId = 1, Quantity = 1 }
                }
            };

            // Act:

            var invoice = await invoiceService.GetInvoice(request);

            // Assert:

            Assert.IsType<Invoice>(invoice);
            Assert.Null(invoice.Products.First().VATSum);
        }
    }
}