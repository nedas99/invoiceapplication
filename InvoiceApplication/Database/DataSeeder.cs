﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Models.Database;
using Models.Enums;

namespace Database
{
    public static class DataSeeder
    {
        public static async Task Seed(IServiceProvider service)
        {
            var context = service.GetRequiredService<DataContext>();
            context.Database.Migrate();
            await AddProducts(context);
            await AddPeople(context);
        }

        private static async Task AddProducts(DataContext context)
        {
            if (context.Products.Any())
            {
                return;
            }

            context.Products.Add(new Product { Title = "Notebook", PriceWithoutVAT = 1.5m, VAT = VAT.TwentyOne });
            context.Products.Add(new Product { Title = "Dictionary", PriceWithoutVAT = 129.99m, VAT = VAT.Nine });
            context.Products.Add(new Product { Title = "Newspaper", PriceWithoutVAT = 1.49m, VAT = VAT.Five });
            context.Products.Add(new Product { Title = "Lottery ticket", PriceWithoutVAT = 2m, VAT = VAT.Zero });

            await context.SaveChangesAsync();
        }

        private static async Task AddPeople(DataContext context)
        {
            if (context.People.Any())
            {
                return;
            }

            context.People.Add(new LegalPerson { Name = "UAB Imone Vienas", Address = "Imones g. 1", Code = "3001111111", VATCode = "LT100001111111" });
            context.People.Add(new LegalPerson { Name = "UAB Imone Du", Address = "Imones g. 2", Code = "3001111112", VATCode = "LT100001111112" });
            context.People.Add(new LegalPerson { Name = "UAB Imone Trys", Address = "Imones g. 3", Code = "3001111113", VATCode = "LT100001111113" });

            context.People.Add(new Individual { Name = "Jonas Jonaitis", Address = "Asmens g. 1", Code = "3009999991", VATCode = "LT100009999991" });
            context.People.Add(new Individual { Name = "Petras Petraitis", Address = "Asmens g. 2", Code = "3009999992", VATCode = "LT100009999992" });

            await context.SaveChangesAsync();
        }
    }
}
