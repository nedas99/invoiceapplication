﻿using Microsoft.EntityFrameworkCore;
using Models.Database;

namespace Database
{
    public class DataContext : DbContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
        }

        public DbSet<Product> Products { get; set; }
        public DbSet<Person> People { get; set; }
        public DbSet<Individual> Individuals { get; set; }
        public DbSet<LegalPerson> LegalPeople { get; set; }
    }
}
