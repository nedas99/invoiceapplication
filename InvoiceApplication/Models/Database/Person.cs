﻿using Models.Base;

namespace Models.Database
{
    public abstract class Person : IEntity
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public string VATCode { get; set; }

        public Person()
        {

        }
    }
}
