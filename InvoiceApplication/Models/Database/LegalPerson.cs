﻿using Models.Api.Invoices;

namespace Models.Database
{
    public class LegalPerson : Person
    {
        public LegalPerson() : base()
        {

        }

        public Invoice CreateInvoice(Person buyer, IEnumerable<Product> products, IEnumerable<ProductAmountDTO> productAmounts)
        {
            var buyerInfo = new PersonInformation(buyer);
            var sellerInfo = new PersonInformation(this);

            var productDtos = new List<InvoiceProductDTO>();
            foreach (var product in products)
            {
                var quantity = productAmounts.First(x => x.ProductId == product.Id).Quantity;
                productDtos.Add(new InvoiceProductDTO(product, quantity));
            }
            var totalVatSum = productDtos.Sum(p => p.VATSum.Value);
            var totalSum = productDtos.Sum(p => p.SumWithVAT);

            var invoice = new Invoice
            {
                Buyer = buyerInfo,
                Seller = sellerInfo,
                Date = DateOnly.FromDateTime(DateTime.UtcNow).ToString(),
                Products = productDtos,
                TotalVATSum = totalVatSum,
                TotalSum = totalSum
            };

            return invoice;
        }
    }
}
