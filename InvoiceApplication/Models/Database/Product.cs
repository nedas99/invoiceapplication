﻿using Models.Base;
using Models.Enums;

namespace Models.Database
{
    public class Product : IEntity
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal PriceWithoutVAT { get; set; }
        public VAT VAT { get; set; }

        public Product()
        {

        }
    }
}
