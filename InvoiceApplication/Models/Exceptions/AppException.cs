﻿namespace Models.Exceptions
{
    public class AppException : Exception
    {
        public int StatusCode { get; set; }
        public string Message { get; set; }

        public AppException(int statusCode, string message)
        {
            StatusCode = statusCode;
            Message = message;
        }

        public static AppException InvalidSeller()
        {
            return new AppException(400, "Seller has to be a legal person");
        }

        public static AppException ProductNotFound()
        {
            return new AppException(404, "Product not found");
        }

        public static AppException PersonNotFound()
        {
            return new AppException(404, "Person not found");
        }

        public static AppException IncorrectVATValue()
        {
            return new AppException(400, "Incorrect VAT value");
        }

        public static AppException EmptyRequestProductList()
        {
            return new AppException(400, "At least one product must be requested");
        }

        public static AppException InvalidQuantity()
        {
            return new AppException(400, "Quantity has to be a positive integer");
        }
    }
}
