﻿using Models.Enums;
using Models.Exceptions;

namespace Models.Extensions
{
    public static class EnumExtensions
    {
        public static int VATTariff(this VAT vat)
        {
            return vat switch
            {
                VAT.Zero => 0,
                VAT.Five => 5,
                VAT.Nine => 9,
                VAT.TwentyOne => 21,
                _ => throw AppException.IncorrectVATValue()
            };
        }
    }
}
