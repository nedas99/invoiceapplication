﻿namespace Models.Base
{
    public interface IEntity
    {
        public int Id { get; set; }
    }
}
