﻿using Models.Database;

namespace Models.Api.People
{
    public class PersonDTO
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public string VATCode { get; set; }
        public bool IsIndividual { get; set; }

        public PersonDTO(Person person)
        {
            Id = person.Id;
            Name = person.Name;
            Address = person.Address;
            Code = person.Code;
            VATCode = person.VATCode;
            IsIndividual = person is Individual;
        }
    }
}
