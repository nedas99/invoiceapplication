﻿using Models.Database;
using Models.Extensions;

namespace Models.Api.Invoices
{
    public class InvoiceProductDTO
    {
        public string Title { get; set; }
        public int Quantity { get; set; }
        public decimal PriceWithoutVAT { get; set; }
        public decimal SumWithoutVAT { get; set; }
        public int VATTariff { get; set; }
        public decimal? VATSum { get; set; }
        public decimal SumWithVAT { get; set; }

        public InvoiceProductDTO(Product product, int quantity)
        {
            Title = product.Title;
            Quantity = quantity;
            PriceWithoutVAT = product.PriceWithoutVAT;
            SumWithoutVAT = PriceWithoutVAT * quantity;
            VATTariff = product.VAT.VATTariff();
            VATSum = decimal.Round(SumWithoutVAT * VATTariff / 100m, 2);
            SumWithVAT = SumWithoutVAT + VATSum.Value;
        }
    }
}
