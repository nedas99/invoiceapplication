﻿namespace Models.Api.Invoices
{
    public class Invoice
    {
        public string Date { get; set; }
        public PersonInformation Seller { get; set; }
        public PersonInformation Buyer { get; set; }
        public IEnumerable<InvoiceProductDTO> Products { get; set; }
        public decimal TotalVATSum { get; set; }
        public decimal TotalSum { get; set; }

        public Invoice()
        {
        }

        public void RemoveProductVATSum()
        {
            foreach (var product in Products)
            {
                product.VATSum = null;
            }
        }
    }
}
