﻿using Models.Exceptions;

namespace Models.Api.Invoices
{
    public class InvoiceRequest
    {
        public int SellerId { get; set; }
        public int BuyerId { get; set; }
        public IEnumerable<ProductAmountDTO> Products { get; set; }

        public void Validate()
        {
            if (!Products.Any())
            {
                throw AppException.EmptyRequestProductList();
            }

            if (Products.Where(p => p.Quantity <= 0).Any())
            {
                throw AppException.InvalidQuantity();
            }
        }
    }
}
