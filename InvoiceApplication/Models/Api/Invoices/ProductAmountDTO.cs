﻿namespace Models.Api.Invoices
{
    public class ProductAmountDTO
    {
        public int ProductId { get; set; }
        public int Quantity { get; set; }
    }
}
