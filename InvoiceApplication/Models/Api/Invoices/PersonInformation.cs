﻿using Models.Database;

namespace Models.Api.Invoices
{
    public class PersonInformation
    {
        public string Name { get; set; }
        public string Address { get; set; }
        public string Code { get; set; }
        public string VATCode { get; set; }

        public PersonInformation(Person person)
        {
            Name = person.Name;
            Address = person.Address;
            Code = person.Code;
            VATCode = person.VATCode;
        }
    }
}
