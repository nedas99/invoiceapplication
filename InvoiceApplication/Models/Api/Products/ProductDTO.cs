﻿using Models.Database;

namespace Models.Api.Products
{
    public class ProductDTO
    {
        public int Id { get; set; }
        public string Title { get; set; }
        public decimal PriceWithoutVAT { get; set; }
        public string VAT { get; set; }

        public ProductDTO(Product product)
        {
            Id = product.Id;
            Title = product.Title;
            PriceWithoutVAT = product.PriceWithoutVAT;
            VAT = product.VAT.ToString();
        }
    }
}
