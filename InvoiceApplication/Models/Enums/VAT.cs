﻿namespace Models.Enums
{
    public enum VAT
    {
        Zero,
        Five,
        Nine,
        TwentyOne
    }
}
