﻿using Models.Api.People;

namespace Services.Interfaces
{
    public interface IPersonService
    {
        Task<IEnumerable<PersonDTO>> GetPeople();
    }
}
