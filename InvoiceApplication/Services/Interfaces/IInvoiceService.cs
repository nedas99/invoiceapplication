﻿using Models.Api.Invoices;

namespace Services.Interfaces
{
    public interface IInvoiceService
    {
        Task<Invoice> GetInvoice(InvoiceRequest request);
    }
}
