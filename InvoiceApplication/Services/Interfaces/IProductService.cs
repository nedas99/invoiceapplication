﻿using Models.Api.Products;
using Models.Database;

namespace Services.Interfaces
{
    public interface IProductService
    {
        Task<IEnumerable<ProductDTO>> GetProductDTOs();
    }
}
