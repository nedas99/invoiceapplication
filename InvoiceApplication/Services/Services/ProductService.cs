﻿using Models.Api.Products;
using Repositories.Interfaces;
using Services.Interfaces;

namespace Services.Services
{
    public class ProductService : IProductService
    {
        private readonly IProductRepository _productRepository;

        public ProductService(IProductRepository productRepository)
        {
            _productRepository = productRepository;
        }

        public async Task<IEnumerable<ProductDTO>> GetProductDTOs()
        {
            var products = await _productRepository.GetAll();
            return products.Select(p => new ProductDTO(p));
        }
    }
}
