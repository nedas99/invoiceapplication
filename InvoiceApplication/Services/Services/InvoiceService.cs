﻿using Models.Api.Invoices;
using Models.Database;
using Models.Exceptions;
using Repositories.Interfaces;
using Services.Interfaces;

namespace Services.Services
{
    public class InvoiceService : IInvoiceService
    {
        private readonly IPersonRepository _personRepository;
        private readonly IProductRepository _productRepository;

        public InvoiceService(IPersonRepository personRepository, IProductRepository productRepository)
        {
            _personRepository = personRepository;
            _productRepository = productRepository;
        }

        public async Task<Invoice> GetInvoice(InvoiceRequest request)
        {
            request.Validate();

            var potentialSeller = await _personRepository.GetById(request.SellerId)
                ?? throw AppException.PersonNotFound();

            if (potentialSeller is not LegalPerson seller)
            {
                throw AppException.InvalidSeller();
            }

            var buyer = await _personRepository.GetById(request.BuyerId)
                ?? throw AppException.PersonNotFound();

            var productIds = request.Products.Select(p => p.ProductId).ToList();
            var products = await _productRepository.GetProductsByIds(productIds);

            if (request.Products.Count() != products.Count())
            {
                throw AppException.ProductNotFound();
            }

            var invoice = seller.CreateInvoice(buyer, products, request.Products);
            
            if (buyer is LegalPerson)
            {
                invoice.RemoveProductVATSum();
            }

            return invoice;
        }
    }
}
