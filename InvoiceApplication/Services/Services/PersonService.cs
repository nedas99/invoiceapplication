﻿using Models.Api.People;
using Repositories.Interfaces;
using Services.Interfaces;

namespace Services.Services
{
    public class PersonService : IPersonService
    {
        private readonly IPersonRepository _personRepository;

        public PersonService(IPersonRepository personRepository)
        {
            _personRepository = personRepository;
        }

        public async Task<IEnumerable<PersonDTO>> GetPeople()
        {
            var people = await _personRepository.GetAll();
            return people.Select(p => new PersonDTO(p));
        }
    }
}
