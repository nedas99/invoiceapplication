﻿using Models.Database;

namespace Repositories.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        Task<IEnumerable<Product>> GetProductsByIds(IEnumerable<int> ids);
    }
}
