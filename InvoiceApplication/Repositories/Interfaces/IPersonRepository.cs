﻿using Models.Database;

namespace Repositories.Interfaces
{
    public interface IPersonRepository : IBaseRepository<Person>
    {
    }
}
