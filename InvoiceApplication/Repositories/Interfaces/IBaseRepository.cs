﻿using Models.Base;

namespace Repositories.Interfaces
{
    public interface IBaseRepository<T> where T : class, IEntity
    {
        Task Add(T entity);
        Task<T> GetById(int id);
        Task Update(T entity);
        Task Delete(T entity);
        Task<IEnumerable<T>> GetAll();
    }
}
