﻿using Database;
using Models.Database;
using Repositories.Interfaces;

namespace Repositories.Repositories
{
    public class PersonRepository : BaseRepository<Person>, IPersonRepository
    {
        public PersonRepository(DataContext context) : base(context)
        {
        }
    }
}
