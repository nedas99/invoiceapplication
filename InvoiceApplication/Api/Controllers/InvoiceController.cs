﻿using Microsoft.AspNetCore.Mvc;
using Models.Api.Invoices;
using Services.Interfaces;

namespace Api.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InvoiceController : ControllerBase
    {
        private readonly IInvoiceService _invoiceService;

        public InvoiceController(IInvoiceService invoiceService)
        {
            _invoiceService = invoiceService;
        }

        [HttpPost]
        public async Task<IActionResult> GetInvoice([FromBody] InvoiceRequest request)
            => Ok(await _invoiceService.GetInvoice(request));
    }
}
