﻿using Models.Api;
using Models.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Serialization;

namespace Api.Middlewares
{
    public class ExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly JsonSerializerSettings settings;

        public ExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next;
            settings = new JsonSerializerSettings
            {
                ContractResolver = new DefaultContractResolver
                {
                    NamingStrategy = new CamelCaseNamingStrategy()
                }
            };
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                await _next(context);
            }
            catch (AppException e)
            {
                await SetResponseBody(context, e.Message, e.StatusCode);
                return;

            }
            catch (Exception e)
            {
                await SetResponseBody(context, e.Message, 400);
                return;
            }
        }

        private async Task SetResponseBody(HttpContext context, string message, int statusCode)
        {
            var response = new ErrorResponse(message);

            context.Response.ContentType = "application/json";
            context.Response.StatusCode = statusCode;
            var jsonObject = JsonConvert.SerializeObject(response, settings);
            await context.Response.WriteAsync(jsonObject);
        }
    }
}
